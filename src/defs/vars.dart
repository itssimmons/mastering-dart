/** Declaring variables */

// -- auto typed variables
var name = 'John';

// -- typed variable
Object lastName = 'Bob';

// -- int | null 
int? number;
assert(number == null); // true

/** `late` variables
  * Declaring a non-nullable variable that’s initialized after its declaration.
  * Lazily initializing a variable.
  * The variable might not be needed, and initializing it is costly.
  * You’re initializing an instance variable, and its initializer needs access to this.
  */
late String description

void main() {
  description = 'Hello world!';
  print(description)
}

/** `final` and `const` variables
  * If you never intend to change a variable, use final or const, either instead of var or in addition to a type.
  * A final variable can be set only once; a const variable is a compile-time constant. (Const variables are implicitly final.)
  */
final username = 'Bob'; // Without a type annotation
final String nickname = 'Bobby';
username = 'Alice'; // Error: a final variable can only be set once.


/**
  * Use const for variables that you want to be compile-time constants. If the const variable is at the class level, mark it static const.
  */
var foo = const [];
final bar = const [];
const baz = []; // Equivalent to `const []`

foo = [1, 2, 3]; // Was const [] 
// You can’t change the value of a const variable:
baz = [42]; // Error: Constant variables can't be assigned a value.

/** You can define constants that use type checks and casts (is and as), collection if, and spread operators (... and ...?): */

const Object i = 3; // Where i is a const Object with an int value...
const list = [i as int]; // Use a typecast.
const map = {if (i is int) i: 'int'}; // Use is and collection if.
const set = {if (list is List<int>) ...list}; // ...and a spread.