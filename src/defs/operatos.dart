/**
  * unary postfix	            | expr++    expr--    ()    []    ?[]    .    ?.    !
  * unary prefix	            | -expr    !expr    ~expr    ++expr    --expr      await expr
  * multiplicative	          | *    /    %   ~/
  * additive                  |	+    -
  * relational and type test	| >=    >    <=    <    as    is    is!
  * equality                  |	==    !=   
  * logical AND	              | &&
  * logical OR	              | ||
  * if null                   |	??
  * conditional               |	expr1 ? expr2 : expr3
  * cascade                   |	..    ?..
  * assignment                |	=    *=    /=   +=   -=   &=   ^=   etc.
  */

a++
a + b
a = b
a == b
c ? a : b
a is T

assert(2 + 3 == 5);
assert(2 - 3 == -1);
assert(2 * 3 == 6);
assert(5 / 2 == 2.5); // Result is a double
assert(5 ~/ 2 == 2); // Result is an int
assert(5 % 2 == 1); // Remainder

assert('5/2 = ${5 ~/ 2} r ${5 % 2}' == '5/2 = 2 r 1');

int a;
int b;

a = 0;
b = ++a; // Increment a before b gets its value.
assert(a == b); // 1 == 1

a = 0;
b = a++; // Increment a AFTER b gets its value.
assert(a != b); // 1 != 0

a = 0;
b = --a; // Decrement a before b gets its value.
assert(a == b); // -1 == -1

a = 0;
b = a--; // Decrement a AFTER b gets its value.
assert(a != b); // -1 != 0

/**
  * If the object that the cascade operates on can be null, then use a null-shorting cascade (?..) for the first operation.
  * Starting with ?.. guarantees that none of the cascade operations are attempted on that null object.
*/
querySelector('#confirm') // Get an object.
  ?..text = 'Confirm' // Use its members.
  ..classes.add('important')
  ..onClick.listen((e) => window.alert('Confirmed!'))
  ..scrollIntoView();
