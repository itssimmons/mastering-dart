import 'dart:html';
import 'package:test/test.dart';
import 'package:lib1/lib1.dart';
import 'package:lib2/lib2.dart' as lib2;

// Uses Element from lib1.
Element element1 = Element();

// Uses Element from lib2.
lib2.Element element2 = lib2.Element();

// Import only foo.
import 'package:lib1/lib1.dart' show foo;

// Import all names EXCEPT foo.
import 'package:lib2/lib2.dart' hide foo;

/** Lazily loading a library */
import 'package:greetings/hello.dart' deferred as hello;
// When you need the library, invoke loadLibrary() using the library’s identifier.

Future<void> greet() async {
  await hello.loadLibrary();
  hello.printGreeting();
}

/**
  * The library directive
  * To specify library-level doc comments or metadata annotations, attach them to a library declaration at the start of the file.
  */
/// A really great test library.
@TestOn('browser')
library;

/**
  * Creating a new package
  * `$ dart create -t package <PACKAGE_NAME>`
  */